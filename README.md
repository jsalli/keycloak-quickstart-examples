# Setting things up
Copy Keycloak quickstart files from Github to 'wildfly-files/keycloak-quickstarts-4.8.3.Final'
[keycloak-quickstarts 4.8.3!](https://github.com/keycloak/keycloak-quickstarts/tree/4.8.3.Final)

### Copy files and folders from Keycloak Github to build our own Keycloak Docker image
Get everything except the 'Dockerfile' from here:
[keycloak Docker container building!](https://github.com/jboss-dockerfiles/keycloak/tree/4.8.3.Final/server)

### Build the docker images
Run at project's root folder

    docker-compose build

### Start docker-compose

    docker-compose up

### Connect to the wildfly keycloak quickstart container

    docker exec -it wildfly_kctest_container bash

### Start the WildFly server in frist terminal

    /opt/jboss/wildfly/bin/standalone.sh -b 0.0.0.0 -bmanagement 0.0.0.0

if there is an error showing something with "can't rename / directory not empty exeption:
rm all from /opt/jboss/wildfly/standalone/configuration/standalone_xml_history/current

### Connect to the container again 

    docker exec -it wildfly_kctest_container bash

### Install the Keycloak adapter

    /opt/jboss/wildfly/bin/jboss-cli.sh -c --file=/opt/jboss/wildfly/bin/adapter-install.cli
    /opt/jboss/wildfly/bin/jboss-cli.sh -c --command=:reload

### Rename keycloak server from inside the selected example project
http://localhost:8180 -> http://keycloak:8180

### Deploy the photos uma example

    cd app-authz-uma-photoz/photoz-html5-client/
    mvn clean wildfly:deploy
    cd ../photoz-restful-api
    mvn clean package wildfly:deploy

### In Windows add keycloak to hosts-file
Add below line to hosts-file at **C:\Windows\System32\drivers\etc**

    127.0.0.1       keycloak


# Other resources
### Install maven
https://linuxize.com/post/how-to-install-apache-maven-on-centos-7/